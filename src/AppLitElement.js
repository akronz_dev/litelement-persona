import { LitElement, html, css } from 'lit-element';
import  './components/ficha-persona';

export class AppLitElement extends LitElement {
  static get properties() {
    return {
      persona: { type: Object }
    };
  }

   constructor() {
     super();
     this.persona = {
       nombre: 'Jose Arturo Sanchez Soto',
       email: 'jose.sanchez.soto@bbva.com',
       phone: '969007452',
       yearsInCompany:2,
       photo: './assets/images/avatar.png'
     };
   }

  static get styles() {
    return css`
      :host {
        display: flex;
        box-sizing: border-box;
        align-items: center;
        justify-content: center;
        min-height: 100vh;
      }

    `;
  }

  render() {
    return html`
      <ficha-persona
        .persona = "${this.persona}"></ficha-persona>
    `;
  }
}
