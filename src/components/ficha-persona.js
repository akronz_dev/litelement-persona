import { LitElement, html, css } from 'lit-element';
import '@polymer/iron-icon/iron-icon.js';
import '@polymer/iron-icons/iron-icons.js';
import '@polymer/iron-icons/communication-icons';
import '@polymer/iron-icons/editor-icons';
import '@polymer/iron-icons/social-icons';
import '@polymer/iron-icons/image-icons';

class FichaPersona extends LitElement {

  static get is() {
    return 'ficha-persona';
  }

  static get styles() {
    return css`

        :root {
          font-size: 16px;
        }
       .content-form {
        position: relative;
        display: flex;
        flex-direction: column;
        background-color: #fff;
        border: 1px solid #e1e1e1;
        padding: 15px 30px;
        align-items: center;
      }
      h1 {
        font-size: 1.8em;
        margin:0.2em;
        text-align: center;
      }

      span {
        font-family: 'Lato', sans-serif;
        color: #666;
        line-height: 2em;
      }

      img {
        margin-bottom: 10px;
        margin-top: 10px;
        width: 220px;
        border-radius: 50%;
        border: 2px solid #e1e1e1;
      }

      .separator {
        position: relative;
        width: 100%;
        margin-top:25px;
        margin-bottom: 15px;
        border-bottom: 1px solid #e1e1e1;
      }

      .btn-edit {
        position: absolute;
        left: calc(50% - 40px);
        top: -15px;
        font-size: 0.8em;
        padding: 7px 12px;
        background-color: #00897b;
        color: #fff;
        outline: none;
        border: none;
        border-radius: 5px;
        cursor: pointer;
        width: 80px;
      }

      .btn-edit:hover {
        background-color: #00796b;
      }

      .btn-edit iron-icon{
        width: 16px;
        height: 16px;
      }

      .content-form-edit {
        margin-top: 10px;
        width: calc(100% - 30px);
        background-color: #f4f4f4;
        padding: 8px 15px;
      }

      .content-form-edit label {
        width: 100%;
        font-weight: bold;
        font-size: 0.8em;
        line-height:2em;
      }

      .content-form-edit input {
       width: calc(100% - 35px);
       padding: 10px 0 8px 35px;
       border: none;
       border-bottom: 1px solid #e1e1e1;
       outline: none;
       font-size: 1em;
       color: #666;
       background-color: #fff;
       margin-bottom:5px;
      }

      .form-group iron-icon {
        position: absolute;
        left: 7px;
        bottom: 12px;
        width: 22px;
        height: 22px;
        color: #666;
      }

      .content-contact {
        display: flex;
      }

      .content-contact .phone{
        width: 58%;

      }

      .content-contact .years-work{
        width: 42%;
        margin-left: 10px;
      }


      .form-group {
        position: relative;
      }

      .hide {
        display: none;
      }

      .btn-cancel {
        background-color: #455a64;
      }

      .btn-cancel:hover {
        background-color: #37474f;
      }

    `
  }

  static get properties() {
    return {
      persona: { type: Object },
      textNotAvailable: { type: String },
      editState: { type: Boolean }
    };
  }

  constructor() {
    super();
    this.persona = {};
    this.textNotAvailable = 'not available';
  }

  updated(changedProps) {
    changedProps.forEach((oldValue, propName) => {
      console.log(`La propiedad ${propName} cambio de: ${typeof oldValue === 'object' ? JSON.stringify(oldValue) : oldValue}, a su nuevo valor ${typeof this[propName] === 'object' ? JSON.stringify(this[propName]) : this[propName]}`);
    });
  }

  updateInputInformation(value, prop) {
    let tmp = { ...this.persona };
    tmp[prop] = value;
    this.persona = tmp;
    this.requestUpdate();
  }

  updateProfile() {
    if (this.persona) {
      if (this.persona.yearsInCompany <= 2) {
        return 'Junior Specialist';
      } else if (this.persona.yearsInCompany > 2 && this.persona.yearsInCompany < 5) {
        return 'Senior Specialist';
      } else {
        return 'Expert Specialist';
      }
    }
    return 'Junior Specialist';
  }

  onEdit() {
    this.editState = !this.editState;
    this.shadowRoot.querySelector('.content-form-edit').classList.toggle('hide');
  }

  render() {
    return html`
      <div class="content-form">
        <h1>${this.persona.nombre}</h1>
        <img src="${this.persona.photo}">
        <span>
          <iron-icon icon="mail"></iron-icon> ${this.persona.email ? this.persona.email : this.textNotAvailable}
        </span>
        <span>
          <iron-icon icon="work"></iron-icon> ${this.updateProfile()}
        </span>
        <span>
          <iron-icon icon="communication:phone"></iron-icon> ${this.persona.phone ? this.persona.phone :
          this.textNotAvailable}
        </span>
        <div class="separator">
          <button @click="${this.onEdit}" class="btn-edit${this.editState ? ' btn-cancel' : ''}">${this.editState ?
            'Cancelar' : 'Editar'}</button>
        </div>
        <div class="content-form-edit hide">
          <div class="form-group">
            <label>Nombre</label>
            <input value="${this.persona.nombre}" @input="${(e) => this.updateInputInformation(e.target.value, 'nombre')}">
            <iron-icon icon="social:people"></iron-icon>
          </div>
          <div class="form-group">
            <label>Email</label>
            <input value="${this.persona.email}" @input="${(e) => this.updateInputInformation(e.target.value, 'email')}">
            <iron-icon icon="mail"></iron-icon>
          </div>
          <div class="content-contact">
            <div class="phone form-group">
              <label>Teléfono</label>
              <input value="${this.persona.phone}" @input="${(e) => this.updateInputInformation(e.target.value, 'phone')}">
              <iron-icon icon="communication:phone"></iron-icon>
            </div>
            <div class="years-work form-group">
              <label>Años trabajando</label>
              <input min="0" type="number" value="${this.persona.yearsInCompany}"
                @input="${(e) => this.updateInputInformation(e.target.value, 'yearsInCompany')}">
              <iron-icon icon="work"></iron-icon>
            </div>
          </div>
          <div class="form-group">
            <label>Foto</label>
            <input value="${this.persona.photo}" @input="${(e) => this.updateInputInformation(e.target.value, 'photo')}">
            <iron-icon icon="image:image"></iron-icon>
          </div>
        </div>

      </div>

    `
  }
}
customElements.define(FichaPersona.is, FichaPersona);